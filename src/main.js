import { createApp } from "vue";
import App from "@/App.vue";
import router from "@/router";
import store from "@/store";
import BaseButton from "@/components/ui/BaseButton.vue";
import BaseCard from "@/components/ui/BaseCard.vue";
import BaseDialog from "@/components/ui/BaseDialog.vue";
import BaseSpinner from "@/components/ui/BaseSpinner.vue";
import ImageUploadVue from "image-upload-vue";

const app = createApp(App).use(store).use(router).use(ImageUploadVue);

app.component("base-button", BaseButton);
app.component("base-card", BaseCard);
app.component("base-dialog", BaseDialog);
app.component("base-spinner", BaseSpinner);

app.mount("#app");
