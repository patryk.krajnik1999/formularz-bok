import { createRouter, createWebHistory } from "vue-router";
import ReportForm from "@/pages/AddReport.vue";
import ReportsList from "@/pages/ReportsList.vue";
import ReportDetails from "@/pages/ReportDetails.vue";
import FirstRead from "@/pages/FirstRead.vue";
import NotFound from "@/pages/NotFound.vue";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: "/", redirect: "/reports" },
    { path: "/form", component: ReportForm },
    { path: "/reports", component: ReportsList },
    { path: "/reports/:id", component: ReportDetails, props: true },
    { path: "/firstRead/:id", component: FirstRead, props: true },
    { path: "/:notFouund(.*)", component: NotFound },
  ],
});

export default router;
