import { createRouter, createWebHistory } from "vue-router";
// import HomeView from '../views/HomeView.vue'
import ApplicationForm from "@/pages/AddReport.vue";

const routes = [
  {
    path: "/",
    redirect: "/form",
  },
  {
    path: "/form",
    component: ApplicationForm,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
