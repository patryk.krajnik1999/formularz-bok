export default {
  reports(state) {
    return state.reports;
  },
  units(state) {
    return state.units;
  },
  reportStatutes(state) {
    return state.reportStatutes;
  },
  image(state) {
    return state.image;
  },
};
