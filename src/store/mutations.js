export default {
  setReports(state, payload) {
    state.reports = payload;
  },
  setUnits(state, payload) {
    state.units = payload;
  },
  setReportStatutes(state, payload) {
    state.reportStatutes = payload;
  },

  setFirstRead(state, payload) {
    let report = state.reports.find((report) => report["uuId"] === payload.id);
    report["isRead"] = true;
    report["firstReader"] = payload.firstReader;
    report["firstDate"] = payload.firstDate;
  },

  setStatus(state, payload) {
    let report = state.reports.find(
      (report) => report["uuId"] === payload.reportId
    );
    report["status"] = payload.statusId;
  },

  setImage(state, payload) {
    state.image = payload;
  },

  addComment(state, payload) {
    let report = state.reports.find((report) => report["uuId"] === payload.id);
    report["comments"].push({
      user: payload.user,
      content: payload.content,
    });
  },
};
