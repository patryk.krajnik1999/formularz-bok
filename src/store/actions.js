export default {
  async loadReports(context) {
    const response = await fetch(`http://formularz_bok/reports`);

    if (!response.ok) {
      throw new Error("Nie udało się pobrać danych.");
    }

    const responseData = await response.json();

    const units = [];
    for (const res in responseData["units"]) {
      const unit = {
        id: responseData["units"][res]["id"],
        title: responseData["units"][res]["title"],
      };
      units.push(unit);
    }

    const reportStatutes = [];
    for (const res in responseData["reportStatutes"]) {
      const reportStatus = {
        id: responseData["reportStatutes"][res]["id"],
        status: responseData["reportStatutes"][res]["status"],
      };
      reportStatutes.push(reportStatus);
    }

    const reports = [];
    for (const res in responseData["reports"]) {
      const report = {
        id: "r" + res,
        uuId: responseData["reports"][res]["id"],
        topic: responseData["reports"][res]["topic"],
        comment: responseData["reports"][res]["comment"],
        email: responseData["reports"][res]["email"],
        phone: responseData["reports"][res]["phone"],
        emailPermission: responseData["reports"][res]["emailPermission"],
        phonePermission: responseData["reports"][res]["phonePermission"],
        userAgent: responseData["reports"][res]["userAgent"],
        unit: responseData["reports"][res]["unit"],
        isRead: responseData["reports"][res]["isRead"],
        firstReader: responseData["reports"][res]["firstReader"],
        firstDate: responseData["reports"][res]["firstDate"],
        status: responseData["reports"][res]["status"],
        comments: responseData["reports"][res]["comments"],
        date: responseData["reports"][res]["date"],
      };
      reports.push(report);
    }

    context.commit("setReports", reports);
    context.commit("setUnits", units);
    context.commit("setReportStatutes", reportStatutes);
  },

  async addReport(context, payload) {
    const newRequest = {
      topic: payload["data"].topic,
      comment: payload["data"].comment,
      unit: payload["data"].unit,
      email: payload["data"].email,
      phone: payload["data"].phone,
      emailPermission: payload["data"].emailPermission,
      phonePermission: payload["data"].phonePermission,
      userAgent: navigator.userAgent,
    };

    const response = await fetch(`http://formularz_bok/newReport`, {
      method: "POST",
      body: JSON.stringify(newRequest),
    });

    if (!response.ok) {
      throw new Error("Bład podczas wysyłania danych.");
    }

    const responseData = await response.json();

    console.log(responseData);
  },

  async addComment(context, payload) {
    const newComment = {
      reportId: payload.reportId,
      content: payload.content,
      user: payload.user,
    };

    const response = await fetch(`http://formularz_bok/newComment`, {
      method: "POST",
      body: JSON.stringify(newComment),
    });

    if (!response.ok) {
      throw new Error("Bład podczas wysyłania danych.");
    }

    const responseData = await response.json();

    context.commit("addComment", {
      id: payload.reportId,
      content: payload.content,
      user: payload.user,
    });

    console.log(responseData);
  },

  async uploadImage(context, payload) {
    console.log(payload["data"].substring(23));
    const response = await fetch(`http://formularz_bok/uploadImage`, {
      method: "POST",
      body: payload["data"].substring(23),
    });

    const responseData = await response;

    if (!response.ok) {
      throw new Error(responseData.message);
    }

    context.commit("setImage", responseData);

    console.log(responseData);
  },

  async setFirstRead(context, payload) {
    const firstRead = {
      id: payload.id,
      user: payload.user,
    };

    const response = await fetch(`http://formularz_bok/firstRead`, {
      method: "POST",
      body: JSON.stringify(firstRead),
    });

    if (!response.ok) {
      throw new Error("Bład podczas wysyłania danych.");
    }

    const responseData = await response.json();

    context.commit("setFirstRead", {
      id: payload.id,
      firstReader: payload.user,
      firstDate: responseData,
    });
  },

  async setStatus(context, payload) {
    const newRequest = {
      reportId: payload.reportId,
      statusId: payload.statusId,
    };

    const response = await fetch(`http://formularz_bok/setStatus`, {
      method: "POST",
      body: JSON.stringify(newRequest),
    });

    if (!response.ok) {
      throw new Error("Bład podczas wysyłania danych.");
    }

    const responseData = await response.json();

    context.commit("setStatus", {
      reportId: payload.reportId,
      statusId: payload.statusId,
    });

    console.log(responseData);
  },
};
